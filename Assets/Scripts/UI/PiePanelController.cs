﻿using AwesomeCharts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiePanelController : MonoBehaviour
{
    public PieChart chart;
    public GameObject panel;

    public void Show(List<Affector> polluters)
    {
        if(panel.activeSelf)
        {
            return;
        }

        panel.SetActive(true);

        PieDataSet pieData = new PieDataSet();

        foreach (Affector a in polluters)
        {
            pieData.AddEntry(new PieEntry(a.GetCurrentEmissions(), a.affectorName, a.affectorColor));
        }

        chart.GetChartData().DataSet = pieData;

        for(int i=0; i<10; i++)
        {
            chart.SelectEntryAtPosition(i);
        }

        chart.SetDirty();
    }

    public void Refresh(List<Affector> polluters)
    {
        PieDataSet pieData = new PieDataSet();
        foreach (Affector a in polluters)
        {
            pieData.AddEntry(new PieEntry(a.GetCurrentEmissions(), a.affectorName, a.affectorColor));
        }
        chart.GetChartData().DataSet = pieData;
        for (int i = 0; i < 10; i++)
        {
            chart.SelectEntryAtPosition(i);
        }
        chart.SetDirty();
    }

    public void Hide()
    {
        panel.SetActive(false);

        chart.GetChartData().DataSet.Entries.Clear();
        chart.SetDirty();
    }
}
