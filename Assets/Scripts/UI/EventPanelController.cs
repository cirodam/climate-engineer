﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EventPanelController : MonoBehaviour
{
    public GameObject panel;

    public TextMeshProUGUI nameText;
    public TextMeshProUGUI typeText;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_EVENT_STARTED, OnEventStarted);
        yield return null;

        //Send messages
    }

    private void OnEventStarted(Message msg)
    {
        EventStartedMessage cMsg = (EventStartedMessage)msg;

        //nameText.text = cMsg.ev.eventName;
        typeText.text = cMsg.ev.eventName;

        if(cMsg.ev.popup == true)
        {
            panel.SetActive(true);
        }
    }

    public void OnAcceptButton()
    {
        panel.SetActive(false);
    }
}
