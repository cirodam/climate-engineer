﻿using AwesomeCharts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public struct LineToggle
{
    public Affector polluter;
    public LineDataSet data;
    public Toggle toggle;
    public ToggleEntry entry;

    public LineToggle(Affector newPolluter, LineDataSet newData, Toggle newToggle, ToggleEntry newEntry)
    {
        polluter = newPolluter;
        data = newData;
        toggle = newToggle;
        entry = newEntry;
    }

    public void SetData(LineDataSet newData)
    {
        data = newData;
    }
}

public class TimePanelController : MonoBehaviour
{
    public GameObject togglePrefab;

    public GameObject panel;
    public LineChart chart;

    public GameObject toggleRoot;
    List<LineToggle> lineToggles;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        lineToggles = new List<LineToggle>();
        yield return null;

        //Register for messages
        yield return null;

        //Send messages
    }

    private LineDataSet GetAffectorData(Affector a)
    {
        LineDataSet ret = new LineDataSet();
        ret.LineColor = a.affectorColor;

        int interval = a.history.Count / (23);
        int step = interval;
        int pos = 0;

        foreach (AffectorSnapshot s in a.history)
        {
            if(step == interval)
            {
                ret.AddEntry(new LineEntry(pos, s.currentEmissions));
                step = 0;
                pos++;
            }
            else
            {
                step++;
            }
        }

        return ret;
    }

    private void SetLabels(Affector a)
    {
        chart.GetChartData().CustomLables.Clear();

        int interval = a.history.Count / (23);
        int step = interval;
        int pos = 0;
        int labelInterval = 2;

        foreach (AffectorSnapshot s in a.history)
        {
            if (step == interval)
            {
                if(labelInterval % 2 == 0)
                {
                    chart.GetChartData().CustomLables.Add(s.month.ToString() + "/" + (s.year - 2000).ToString());
                    labelInterval = 1;
                }
                else
                {
                    labelInterval++;
                }
                step = 0;
                pos++;
            }
            else
            {
                step++;
            }
        }

        if(a.history.Count > 0)
        {
            chart.XAxis.ShouldDrawLabels = true;
        }
    }

    public void Show(List<Affector> poll)
    {
        if(panel.activeSelf)
        {
            return;
        }

        panel.SetActive(true);

        //Create Toggles and Datasets
        foreach(Affector a in poll)
        {
            LineDataSet l = GetAffectorData(a);

            GameObject to = Instantiate(togglePrefab);
            Toggle toggle = to.GetComponent<Toggle>();
            toggle.transform.SetParent(toggleRoot.transform);
            toggle.isOn = false; 

            ToggleEntry entry = to.GetComponent<ToggleEntry>();
            entry.background.color = a.affectorColor;
            entry.titleText.text = a.affectorName;

            LineToggle lt = new LineToggle(a, l, toggle, entry);
            lineToggles.Add(lt);
            toggle.onValueChanged.AddListener((bool val) => { OnToggle(lt); });
        }

        //Populate Chart
        chart.XAxis.MaxAxisValue = 22;
        foreach(LineToggle lt in lineToggles)
        {
            if(lt.toggle.isOn)
            {
                chart.GetChartData().DataSets.Add(lt.data);
            }
        }

        SetLabels(lineToggles[0].polluter);

        chart.SetDirty();
    }

    public void Refresh()
    {
        chart.GetChartData().DataSets.Clear();
        foreach(LineToggle lt in lineToggles)
        {
            lt.SetData(GetAffectorData(lt.polluter));
            if(lt.toggle.isOn)
            {
                chart.GetChartData().DataSets.Add(lt.data);
            }
        }
        SetLabels(lineToggles[0].polluter);
        chart.SetDirty();
    }

    public void Hide()
    {
        panel.SetActive(false);

        foreach (LineToggle lt in lineToggles)
        {
            Destroy(lt.toggle.gameObject);
        }

        lineToggles.Clear();
        chart.GetChartData().DataSets.Clear();
        chart.SetDirty();
    }

    public void OnToggle(LineToggle lt)
    {
        Refresh();
    }
}
