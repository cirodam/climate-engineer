﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WelcomeScreenController : MonoBehaviour
{
    public GameObject screen;

    public Button newGameButton;
    public Button loadGameButton;
    public Button optionsButton;
    public Button exitButton;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_SCREEN_TRANSITION, OnScreenTransition);
        MessagePump.instance.Register(MessageType.MESSAGE_PANEL_SHOWING, OnPanelShowing);
        MessagePump.instance.Register(MessageType.MESSAGE_PANEL_HIDING, OnPanelHiding);
        yield return null;

        //Send messages
    }

    private void OnScreenTransition(Message msg)
    {
        ScreenTransitionMessage cMsg = (ScreenTransitionMessage)msg;

        if(cMsg.screen == Screen.SCREEN_WELCOME)
        {
            if(!screen.activeSelf)
            {
                screen.SetActive(true);
                newGameButton.enabled = true;
                loadGameButton.enabled = true;
                optionsButton.enabled = true;
                exitButton.enabled = true;
            }
        }
        else
        {
            screen.SetActive(false);
        }
    }

    public void OnNewGameButton()
    {
        MessagePump.instance.QueueMsg(new SessionStartedMessage());
        StartCoroutine(screenTransition());
    }

    public void OnPanelShowing(Message msg)
    {
        if(screen.activeSelf)
        {
            newGameButton.enabled = false;
            loadGameButton.enabled = false;
            optionsButton.enabled = false;
            exitButton.enabled = false;
        }
    }

    public void OnPanelHiding(Message msg)
    {
        if (screen.activeSelf)
        {
            newGameButton.enabled = true;
            loadGameButton.enabled = true;
            optionsButton.enabled = true;
            exitButton.enabled = true;
        }
    }

    public void OnLoadButton()
    {

    }

    public void OnOptionsButton()
    {

    }

    public IEnumerator screenTransition()
    {
        yield return new WaitForSeconds(0.10f);
        MessagePump.instance.QueueMsg(new ScreenTransitionMessage(Screen.SCREEN_WORLD));
    }
}
