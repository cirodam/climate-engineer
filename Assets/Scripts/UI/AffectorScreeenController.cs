﻿using AwesomeCharts;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AffectorScreeenController : MonoBehaviour
{
    public GameObject screen;

    public TextMeshProUGUI titleText;
    public TextMeshProUGUI deltaText;

    public GameObject deltaListRoot;
    List<GameObject> deltaList;

    public LineChart chart;
    public GameObject deltaEntryPrefab;

    Affector target;

    public Color emissionsColor;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        deltaList = new List<GameObject>();
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_SCREEN_TRANSITION, OnScreenTransition);
        MessagePump.instance.Register(MessageType.MESSAGE_AFFECTOR_BUTTON_START_HOVER, OnAffectorStartHover);
        MessagePump.instance.Register(MessageType.MESSAGE_AFFECTOR_BUTTON_STOP_HOVER, OnAffectorButtonStopHover);
        MessagePump.instance.Register(MessageType.MESSAGE_NEW_DATE, OnNewDate);
        yield return null;

        //Send messages
    }

    private void OnAffectorStartHover(Message msg)
    {
        AffectorButtonStartHoverMessage cMsg = (AffectorButtonStartHoverMessage)msg;
        target = cMsg.affector;
    }

    private void OnAffectorButtonStopHover(Message msg)
    {
        target = null;
    }

    private void OnScreenTransition(Message msg)
    {
        ScreenTransitionMessage cMsg = (ScreenTransitionMessage)msg;

        if (cMsg.screen == Screen.SCREEN_AFFECTOR)
        {
            screen.SetActive(true);
            if(target != null)
            {
                Populate();
            }
        }
        else
        {
            screen.SetActive(false);
            Clear();
        }
    }

    private void OnNewDate(Message msg)
    {
        if(screen.gameObject.activeSelf)
        {
            Clear();
            Populate();
        }
    }

    private void Populate()
    {
        titleText.text = target.affectorName + " - Monthly Emissions : " + target.currentEmissions;
        deltaText.text = "Monthly Change : " + Mathf.Round(target.currentDelta * 1000) / 1000;

        //Populate deltaList
        foreach (Modifier mod in target.modifiers.Values)
        {
            GameObject g = Instantiate(deltaEntryPrefab);
            DeltaEntry d = g.GetComponent<DeltaEntry>();

            d.nameText.text = mod.modName + " : ";
            d.valueText.text = (Mathf.Round(mod.modDelta * 1000) / 1000).ToString("F3");
            deltaList.Add(g);
            g.transform.SetParent(deltaListRoot.transform);
        }

        //Populate Chart
        LineDataSet emissionsData = new LineDataSet();
        emissionsData.LineColor = emissionsColor;
        chart.XAxis.MaxAxisValue = 22;

        int interval = target.history.Count / (23);

        int step = interval;
        int pos = 0;
        int labelstep = 2;

        foreach (AffectorSnapshot a in target.history)
        {
            if(step == interval)
            {
                emissionsData.AddEntry(new LineEntry(pos, a.currentEmissions));
                step = 0;
                pos++;
                if(labelstep == 2)
                {
                    chart.XAxis.ShouldDrawLabels = true;
                    chart.GetChartData().CustomLables.Add(a.month.ToString() + "/" + (a.year-2000).ToString());
                    labelstep = 1;
                }
                else
                {
                    labelstep++;
                }
            }
            else
            {
                step++;
            }
        }

        chart.GetChartData().DataSets.Add(emissionsData);

        chart.SetDirty();

    }

    private void Clear()
    {
        foreach(GameObject g in deltaList)
        {
            Destroy(g);
        }
        deltaList.Clear();

        chart.GetChartData().DataSets.Clear();
        chart.GetChartData().CustomLables.Clear();

        chart.SetDirty();
    }
}
