﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class LoadPanelController : MonoBehaviour
{
    public GameObject panel;

    public GameObject loadListEntryPrefab;

    public GameObject loadListRoot;
    public List<LoadListEntry> loadList;
    LoadListEntry selectedEntry;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        loadList = new List<LoadListEntry>();
        yield return null;

        //Register for messages
        yield return null;

        //Send messages
    }

    public void Show()
    {
        panel.SetActive(true);
        MessagePump.instance.QueueMsg(new PanelShowingMessage());

        foreach(FileInfo f in LoadController.instance.saveInfoList)
        {
            if(f.Name != "output_log.txt")
            {
                GameObject g = Instantiate(loadListEntryPrefab);

                LoadListEntry l = g.GetComponent<LoadListEntry>();
                l.nameText.text = f.Name;
                l.dateText.text = f.LastAccessTime.ToShortDateString();
                loadList.Add(l);

                g.GetComponent<Button>().onClick.AddListener(() => { OnEntryButton(l); });
                g.transform.SetParent(loadListRoot.transform, false);
            }
        }
    }

    public void Hide()
    {
        panel.SetActive(false);
        MessagePump.instance.QueueMsg(new PanelHidingMessage());

        foreach(LoadListEntry l in loadList)
        {
            Destroy(l.gameObject);
        }

        loadList.Clear();
    }

    public void OnEntryButton(LoadListEntry l)
    {
        if(selectedEntry != null)
        {
            selectedEntry.OnDeselect();
        }

        selectedEntry = l;
        selectedEntry.OnSelect();
    }

    public void OnLoadButton()
    {
        LoadController.instance.LoadSave(selectedEntry.nameText.text);
        Hide();
    }

    public void OnDeleteButton()
    {
        LoadController.instance.DeleteSave(selectedEntry.nameText.text);
        Hide();
        Show();
    }
}
