﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldScreenController : MonoBehaviour
{
    public GameObject screen;

    private void Start()
    {
        MessagePump.instance.Register(MessageType.MESSAGE_SCREEN_TRANSITION, OnScreenTransition);
    }

    private void OnScreenTransition(Message msg)
    {
        ScreenTransitionMessage cMsg = (ScreenTransitionMessage)msg;

        if (cMsg.screen == Screen.SCREEN_WORLD)
        {
            screen.SetActive(true);
        }
        else
        {
            screen.SetActive(false);
        }
    }
}
