﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AffectorHighlightController : MonoBehaviour
{
    public GameObject panel;
    public RectTransform panelTransform;
    public TextMeshProUGUI affectorName;
    public TextMeshProUGUI affectorEmissions;

    bool hovering;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_AFFECTOR_BUTTON_START_HOVER, OnAffectorButtonStartHover);
        MessagePump.instance.Register(MessageType.MESSAGE_AFFECTOR_BUTTON_STOP_HOVER, OnAffectorButtonStopHover);
        MessagePump.instance.Register(MessageType.MESSAGE_SCREEN_TRANSITION, OnScreenTransition);
        yield return null;

        //Send messages
    }

    private void OnScreenTransition(Message msg)
    {
        if(panel.activeSelf)
        {
            panel.SetActive(false);
            hovering = false;
        }
    }

    private void OnAffectorButtonStartHover(Message msg)
    {
        AffectorButtonStartHoverMessage cMsg = (AffectorButtonStartHoverMessage)msg;

        panel.SetActive(true);
        affectorName.text = cMsg.affector.affectorName;
        affectorEmissions.text = cMsg.affector.GetCurrentEmissions().ToString("F3");
        panelTransform.sizeDelta = new Vector2(affectorName.preferredWidth + 50, panelTransform.sizeDelta.y);
        hovering = true;
    }

    private void OnAffectorButtonStopHover(Message msg)
    {
        AffectorButtonStopHoverMessage cMsg = (AffectorButtonStopHoverMessage)msg;

        panel.SetActive(false);

        hovering = false;
    }

    private void Update()
    {
        if(hovering)
        {
            panel.GetComponent<RectTransform>().position = Input.mousePosition;
        }
    }

}
