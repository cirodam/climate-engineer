﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenController : MonoBehaviour
{
    public GameObject screen;
    public Screen screenName;

    private void Start()
    {
        MessagePump.instance.Register(MessageType.MESSAGE_SCREEN_TRANSITION, OnScreenTransition);
    }

    private void OnScreenTransition(Message msg)
    {
        ScreenTransitionMessage cMsg = (ScreenTransitionMessage)msg;

        if(cMsg.screen == screenName)
        {
            screen.SetActive(true);
        }
        else
        {
            screen.SetActive(false);
        }
    }
}
