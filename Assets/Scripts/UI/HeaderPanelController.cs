﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeaderPanelController : MonoBehaviour
{
    public GameObject panel;

    public Text powerText;
    public Text happinessText;
    public Text dateText;

    public Button climateButton;
    public Button policiesButton;
    public Button powerButton;

    string[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_NEW_DATE, OnNewDate);
        MessagePump.instance.Register(MessageType.MESSAGE_POLITICAL_POWER_CHANGE, OnNewPower);
        MessagePump.instance.Register(MessageType.MESSAGE_POLITICAL_POWER_DELTA_CHANGE, OnNewPowerDelta);
        MessagePump.instance.Register(MessageType.MESSAGE_HAPPINESS_CHANGED, OnNewHappiness);
        MessagePump.instance.Register(MessageType.MESSAGE_SCREEN_TRANSITION, OnScreenTransition);
        MessagePump.instance.Register(MessageType.MESSAGE_DATE_SET, OnDateSet);
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_PAUSED, OnGamePaused);
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_RESUMED, OnGameResumed);
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_STARTED, OnSessionStarted);
        yield return null;

        //Send messages
    }

    private void OnSessionStarted(Message msg)
    {
        climateButton.enabled = true;
        policiesButton.enabled = true;
        powerButton.enabled = true;
    }

    private void OnGamePaused(Message msg)
    {
        if(panel.activeSelf)
        {
            climateButton.enabled = false;
            policiesButton.enabled = false;
            powerButton.enabled = false;
        }
    }

    private void OnGameResumed(Message msg)
    {
        if(panel.activeSelf)
        {
            climateButton.enabled = true;
            policiesButton.enabled = true;
            powerButton.enabled = true;
        }
    }

    private void OnScreenTransition(Message msg)
    {
        ScreenTransitionMessage cMsg = (ScreenTransitionMessage)msg;
        
        if(cMsg.screen == Screen.SCREEN_WELCOME)
        {
            panel.SetActive(false);
        }
        else
        {
            if(panel.activeSelf == false)
            {
                panel.SetActive(true);
            }
        }
    }

    private void OnDateSet(Message msg)
    {
        DateSetMessage cMsg = (DateSetMessage)msg;

        dateText.text = months[cMsg.month - 1] + " " + cMsg.year;
    }

    private void OnNewDate(Message msg)
    {
        NewDateMessage cMsg = (NewDateMessage)msg;

        dateText.text = months[cMsg.month - 1] + " " + cMsg.year;
    }

    private void OnNewPower(Message msg)
    {
        PoliticalPowerChangeMessage cMsg = (PoliticalPowerChangeMessage)msg;

        powerText.text = Mathf.Round(cMsg.power) + " (" + cMsg.powerDelta.ToString("F1") + ")";
    }

    private void OnNewPowerDelta(Message msg)
    {
        PoliticalPowerDeltaChangeMessage cMsg = (PoliticalPowerDeltaChangeMessage)msg;

        powerText.text = Mathf.Round(cMsg.power) + " (" + cMsg.powerDelta.ToString("F1") + ")";
    }

    private void OnNewHappiness(Message msg)
    {
        HappinessChangedMessage cMsg = (HappinessChangedMessage)msg;

        happinessText.text = cMsg.happiness.ToString("F1") + "%";
    }
}
