﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DeltaEntry : MonoBehaviour
{
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI valueText;
}
