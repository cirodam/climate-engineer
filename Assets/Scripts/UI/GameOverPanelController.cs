﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameOverPanelController : MonoBehaviour
{
    public GameObject panel;

    public TextMeshProUGUI TitleText;
    public TextMeshProUGUI DescText;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_WON, OnGameWon);
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_LOST, OnGameLost);
        yield return null;

        //Send messages
    }

    private void OnGameWon(Message msg)
    {
        TitleText.text = "You Win!";
        DescText.text = "You decreased the country's monthly greenhouse gas emissions by 80 percent.";
        Show();
    }

    private void OnGameLost(Message msg)
    {
        TitleText.text = "You Lost!";
        DescText.text = "You failed to have 50% public support during the recent election";
        Show();
    }

    public void Show()
    {
        panel.SetActive(true);
    }

    public void Hide()
    {
        panel.SetActive(false);
    }

    public void OnMainMenuButton()
    {
        MessagePump.instance.QueueMsg(new SessionEndedMessage());
    }
}
