﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PowerListEntry : MonoBehaviour
{
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI valueText;
}
