﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SavePanelController : MonoBehaviour
{
    public GameObject panel;

    public InputField inputText;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        yield return null;

        //Register for messages
        yield return null;

        //Send messages
    }

    public void Show()
    {
        panel.SetActive(true);
    }

    public void Hide()
    {
        panel.SetActive(false);
    }

    public void OnInputButton()
    {
        Hide();
        LoadController.instance.SaveGame(inputText.text);
    }
}
