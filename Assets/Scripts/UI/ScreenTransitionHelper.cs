﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenTransitionHelper : MonoBehaviour
{
    public Screen targetScreen;

    public void OnButton()
    {
        MessagePump.instance.QueueMsg(new ScreenTransitionMessage(targetScreen));
    }
}
