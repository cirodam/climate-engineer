﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DateTextController : MonoBehaviour
{
    public Text dateText;

    private void Start()
    {
        MessagePump.instance.Register(MessageType.MESSAGE_NEW_DATE, OnNewDate);
    }

    private void OnNewDate(Message msg)
    {
        NewDateMessage cMsg = (NewDateMessage)msg;

        dateText.text = cMsg.month + "/" + cMsg.year;
    }
}
