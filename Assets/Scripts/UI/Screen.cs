﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Screen
{
    SCREEN_WELCOME,
    SCREEN_WORLD,
    SCREEN_AFFECTOR,
    SCREEN_POLICY,
    SCREEN_ClIMATE,
    SCREEN_POWER
}
