﻿using AwesomeCharts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimateScreenController : MonoBehaviour
{
    public GameObject screen;

    List<Affector> affectors;
    List<Affector> polluters;

    public TimePanelController timePanel;
    public PiePanelController piePanel;
    public SummaryPanelContrller summaryPanel;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        affectors = new List<Affector>();
        polluters = new List<Affector>();
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_SCREEN_TRANSITION, OnScreenTransition);
        MessagePump.instance.Register(MessageType.MESSAGE_AFFECTOR_INITIALIZED, OnAffectorInitialized);
        MessagePump.instance.Register(MessageType.MESSAGE_NEW_DATE, OnNewDate);
        yield return null;

        //Send messages
    }

    private void OnScreenTransition(Message msg)
    {
        ScreenTransitionMessage cMsg = (ScreenTransitionMessage)msg;

        if (cMsg.screen == Screen.SCREEN_ClIMATE)
        {
            if (!screen.activeSelf)
            {
                screen.SetActive(true);
                Populate();
            }
        }
        else
        {
            if (screen.activeSelf)
            {
                screen.SetActive(false);
                Clear();
            }
        }
    }

    private void OnAffectorInitialized(Message msg)
    {
        AffectorInitializedMessage cMsg = (AffectorInitializedMessage)msg;

        summaryPanel.startingEmissions += cMsg.affector.baseEmissions;
        affectors.Add(cMsg.affector);
    }

    private void FindPolluters()
    {
        if (screen.activeSelf)
        {
            polluters.Clear();

            foreach (Affector a in affectors)
            {
                if (a.GetCurrentEmissions() > 0)
                {

                    polluters.Add(a);
                    /*for (int i = 0; i < polluters.Count; i++)
                    {
                        if (a.GetCurrentEmissions() > polluters[i].GetCurrentEmissions())
                        {
                            polluters.Insert(i, a);
                            break;
                        }
                    }

                    if (!polluters.Contains(a))
                    {
                        polluters.Add(a);
                    }*/

                }

            }

            polluters.Sort(SortByEmissions);
        }
    }

    private int SortByEmissions(Affector a, Affector b)
    {
        return b.GetCurrentEmissions().CompareTo(a.GetCurrentEmissions());
    }

    private void OnNewDate(Message msg)
    {
        FindPolluters();
        if(timePanel.panel.activeSelf)
        {
            timePanel.Refresh();
        }

        if(piePanel.panel.activeSelf)
        {
            piePanel.Refresh(polluters);
        }

        if(summaryPanel.panel.activeSelf)
        {
            summaryPanel.Hide();
            summaryPanel.Show(polluters);
        }
    }

    private void Populate()
    {
        FindPolluters();
        timePanel.Show(polluters);
    }

    private void Clear()
    {
        timePanel.Hide();
        piePanel.Hide();
        summaryPanel.Hide();
    }

    public void OnTimeButton()
    {
        timePanel.Show(polluters);
        piePanel.Hide();
        summaryPanel.Hide();
    }

    public void OnPieButton()
    {
        piePanel.Show(polluters);
        timePanel.Hide();
        summaryPanel.Hide();
    }

    public void OnSummaryButton()
    {
        summaryPanel.Show(polluters);
        timePanel.Hide();
        piePanel.Hide();
    }
}
