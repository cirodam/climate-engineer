﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LoadListEntry : MonoBehaviour
{
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI dateText;

    public Color normalColor;
    public Color selectedColor;

    public void OnSelect()
    {
        nameText.color = selectedColor;
        dateText.color = selectedColor;
    }

    public void OnDeselect()
    {
        nameText.color = normalColor;
        dateText.color = normalColor;
    }
}
