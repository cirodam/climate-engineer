﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PolicyListEntry : MonoBehaviour
{
    public Image baseImage;

    public Color baseColor;
    public Color selectedColor;
    public Color enactedColor;

    Policy policy;

    public void SetPolicy(Policy newPolicy)
    {
        policy = newPolicy;

        OnNormal();
    }

    public void OnSelected()
    {
        if (!policy.enacted)
        {
            baseImage.color = selectedColor;
        }
    }

    public void OnNormal()
    {
        if(policy.enacted)
        {
            baseImage.color = enactedColor;
        }
        else
        {
            baseImage.color = baseColor;
        }
    }

    public Policy GetPolicy()
    {
        return policy;
    }
}
