﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PolicyScreenController : MonoBehaviour
{
    public GameObject screen;

    //Policy List Prefabs
    public GameObject policyEntryPrefab;
    public GameObject headerEntryPrefab;

    //List of Policy and Header GameObjects
    public GameObject PolicyListRoot;
    List<GameObject> policyList;

    Dictionary<string, List<Policy>> policies;

    public TextMeshProUGUI pwTitleText;
    public TextMeshProUGUI pwCostText;
    public TextMeshProUGUI pwHapinessText;
    public TextMeshProUGUI pwDescText;

    public GameObject enactButton;
    public GameObject cancelButton;

    PolicyListEntry targetPolicy;

    private void Start()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        policies = new Dictionary<string, List<Policy>>();
        policyList = new List<GameObject>();
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_SCREEN_TRANSITION, OnScreenTransition);
        MessagePump.instance.Register(MessageType.MESSAGE_POLICY_INITIALIZED, OnPolicyInitialized);
        MessagePump.instance.Register(MessageType.MESSAGE_NEW_DATE, OnNewDate);
        yield return null;

        //Send messages
    }

    private void OnNewDate(Message msg)
    {
        PopulatePolicyPanel();
    }

    private void OnScreenTransition(Message msg)
    {
        ScreenTransitionMessage cMsg = (ScreenTransitionMessage)msg;

        if (cMsg.screen == Screen.SCREEN_POLICY)
        {
            if(!screen.activeSelf)
            {
                screen.SetActive(true);
                Populate();
            }
        }
        else
        {
            screen.SetActive(false);
            Clear();
        }
    }

    private void OnPolicyInitialized(Message msg)
    {
        PolicyInitializedMessage cMsg = (PolicyInitializedMessage)msg;

        if(!policies.ContainsKey(cMsg.policy.categoryName))
        {
            policies.Add(cMsg.policy.categoryName, new List<Policy>());
        }

        policies[cMsg.policy.categoryName].Add(cMsg.policy);
    }

    private void Populate()
    {
        foreach(string header in policies.Keys)
        {
            GameObject headGO = (GameObject)Instantiate(headerEntryPrefab);
            headGO.GetComponentInChildren<TextMeshProUGUI>().text = header;
            headGO.transform.SetParent(PolicyListRoot.transform, false);
            policyList.Add(headGO);

            foreach(Policy p in policies[header])
            {
                GameObject polGO = (GameObject)Instantiate(policyEntryPrefab);
                polGO.GetComponentInChildren<TextMeshProUGUI>().text = p.policyName;
                polGO.transform.SetParent(PolicyListRoot.transform);
                polGO.GetComponent<Button>().onClick.AddListener(() => { OnPolicyButton(polGO); });
                polGO.GetComponent<PolicyListEntry>().SetPolicy(p);
                policyList.Add(polGO);
            }
        }
        
        //find the first
        foreach(GameObject g in policyList)
        {
            PolicyListEntry p = g.GetComponent<PolicyListEntry>();
            if(p != null)
            {
                OnPolicyButton(g);
                break;
            }
        }
    }

    private void Clear()
    {
        foreach(GameObject g in policyList)
        {
            Destroy(g);
        }

        policyList.Clear();
    }

    private void OnPolicyButton(GameObject pressed)
    {
        //If we already have a targetPolicy
        if(targetPolicy != null)
        {
            targetPolicy.OnNormal();
        }

        //Assign our new targetPolicy
        targetPolicy = pressed.GetComponent<PolicyListEntry>();
        targetPolicy.OnSelected();

        //Populate the PolicyPanel
        PopulatePolicyPanel();
    }

    private void PopulatePolicyPanel()
    {
        if(!screen.activeSelf)
        {
            return;
        }

        ResetPolicyPanel();
        
        Policy p = targetPolicy.GetPolicy();

        pwTitleText.text = p.policyName;
        pwCostText.text = p.policyCost.ToString() + " (" + p.deltaCost.ToString() + ")";
        pwHapinessText.text = p.currentSupport.ToString("F1");
        pwDescText.text = p.policyDesc.text;

        if(!p.enacted)
        {
            enactButton.SetActive(true);
        }
        else
        {
            cancelButton.SetActive(true);
        }
    }

    private void ResetPolicyPanel()
    {
        cancelButton.SetActive(false);
        enactButton.SetActive(false);
    }

    public void OnEnactButton()
    {
        if(targetPolicy != null)
        {

            if(PowerController.instance.politicalPower >= targetPolicy.GetPolicy().policyCost)
            {
                targetPolicy.GetPolicy().enacted = true;
                targetPolicy.OnNormal();
                MessagePump.instance.QueueMsg(new PolicyEnactedMessage(targetPolicy.GetPolicy()));
                enactButton.SetActive(false);
                cancelButton.SetActive(true);
            }
        }
    }

    public void OnCancelButton()
    {
        if(targetPolicy != null)
        {
            if(targetPolicy.GetPolicy().enacted == true)
            {
                targetPolicy.GetPolicy().enacted = false;
                targetPolicy.OnSelected();
                MessagePump.instance.QueueMsg(new PolicyCancelledMessage(targetPolicy.GetPolicy()));
                cancelButton.SetActive(false);
                enactButton.SetActive(true);
            }
        }
    }
}
