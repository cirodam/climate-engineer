﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PowerScreenController : MonoBehaviour
{
    public GameObject screen;

    public GameObject powerEntryPrefab;

    public TextMeshProUGUI powerTitleText;
    public GameObject powerListRoot;
    private List<GameObject> powerList;

    public TextMeshProUGUI happinessTitleText;
    public GameObject hapinessListRoot;
    private List<GameObject> hapinessList;

    private bool speechActive;
    private bool fundraiseActive;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        powerList = new List<GameObject>();
        hapinessList = new List<GameObject>();
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_SCREEN_TRANSITION, OnScreenTransition);
        MessagePump.instance.Register(MessageType.MESSAGE_HAPPINESS_CHANGED, OnNewHappiness);
        MessagePump.instance.Register(MessageType.MESSAGE_POLITICAL_POWER_DELTA_CHANGE, OnNewPower);
        MessagePump.instance.Register(MessageType.MESSAGE_EVENT_ENDED, OnEventEnded);
        yield return null;

        //Send messages
    }

    private void OnScreenTransition(Message msg)
    {
        ScreenTransitionMessage cMsg = (ScreenTransitionMessage)msg;

        if(cMsg.screen == Screen.SCREEN_POWER)
        {
            if(!screen.activeSelf)
            {
                screen.SetActive(true);
                Populate();
            }
        }
        else
        {
            screen.SetActive(false);
            Clear();
        }
    }

    private void OnEventEnded(Message msg)
    {
        EventEndedMessage cMsg = (EventEndedMessage)msg;

        if(cMsg.ev.eventName == "Fundraising")
        {
            fundraiseActive = false;
        }
        else if(cMsg.ev.eventName == "Speech")
        {
            speechActive = false;
        }
    }

    private void OnNewHappiness(Message msg)
    {
        if(screen.gameObject.activeSelf)
        {
            Clear();
            Populate();
        }
    }

    private void OnNewPower(Message msg)
    {
        if (screen.gameObject.activeSelf)
        {
            Clear();
            Populate();
        }
    }

    private void Populate()
    {
        //Populate Power
        powerTitleText.text = "Power Change : " + PowerController.instance.politicalPowerDelta.ToString("F1");

        foreach(DeltaMod m in PowerController.instance.deltaMods)
        {
            string val = m.modValue.ToString("F1");
            if(m.modValue >= 0)
            {
                val = "+" + val;
            }

            GameObject g = Instantiate(powerEntryPrefab);
            g.GetComponent<PowerListEntry>().nameText.text = m.modName + " : ";
            g.GetComponent<PowerListEntry>().valueText.text = val;

            powerList.Add(g);
            g.transform.SetParent(powerListRoot.transform);
        }

        //Populate Happiness
        happinessTitleText.text = "Public Support : " + HappinessController.instance.currentHapiness.ToString("F1");

        foreach (Modifier m in HappinessController.instance.modifiers)
        {
            string val = m.modDelta.ToString("F1");

            GameObject g = Instantiate(powerEntryPrefab);
            g.GetComponent<PowerListEntry>().nameText.text = m.modName + " : ";
            g.GetComponent<PowerListEntry>().valueText.text = val;

            hapinessList.Add(g);
            g.transform.SetParent(hapinessListRoot.transform);
        }
    }

    private void Clear()
    {
        foreach(GameObject g in powerList)
        {
            Destroy(g);
        }

        powerList.Clear();

        foreach (GameObject g in hapinessList)
        {
            Destroy(g);
        }

        hapinessList.Clear();
    }

    public void OnFundraiseButton()
    {
        if(!fundraiseActive)
        {
            Event speech = new Event();
            speech.eventName = "Fundraising";
            speech.eventType = "Political";
            speech.turnsRemaining = 12;
            speech.popup = false;

            Modifier m = new Modifier();
            m.modName = "Fundraising";
            m.modDelta = HappinessController.instance.currentHapiness - 20;

            EventModification em = new EventModification("Happiness", m);
            speech.modifications.Add(em);
            MessagePump.instance.QueueMsg(new EventStartedMessage(speech));
            fundraiseActive = true;
        }
    }

    public void OnSpeechButton()
    {
        if(!speechActive)
        {
            if (PowerController.instance.politicalPower >= 400)
            {
                Event speech = new Event();
                speech.eventName = "Speech";
                speech.eventType = "Political";
                speech.turnsRemaining = 12;
                speech.popup = false;

                Modifier m = new Modifier();
                m.modName = "Speech";
                m.modDelta = 100;

                EventModification em = new EventModification("Happiness", m);
                speech.modifications.Add(em);
                MessagePump.instance.QueueMsg(new EventStartedMessage(speech));
                speechActive = true;
            }
        }
    }
}
