﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleEntry : MonoBehaviour
{
    public Text titleText;
    public Image background;
    public Image foreground;
}
