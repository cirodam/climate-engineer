﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausePanelController : MonoBehaviour
{
    public GameObject panel;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_PAUSED, OnGamePaused);
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_RESUMED, OnGameResumed);
        yield return null;

        //Send messages
    }

    private void OnGamePaused(Message msg)
    {
        panel.SetActive(true);
        MessagePump.instance.QueueMsg(new PanelShowingMessage());
    }

    private void OnGameResumed(Message msg)
    {
        if(panel.activeSelf)
        {
            panel.SetActive(false);
            MessagePump.instance.QueueMsg(new PanelHidingMessage());
        }
    }

    public void Hide()
    {
        panel.SetActive(false);
    }

    public void OnMainMenuButton()
    {
        MessagePump.instance.QueueMsg(new SessionEndedMessage());
    }
}
