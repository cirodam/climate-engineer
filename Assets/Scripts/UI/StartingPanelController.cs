﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingPanelController : MonoBehaviour
{
    public GameObject panel;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_STARTED, OnSessionStarted);
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_RESUMED, OnGameResumed);
        yield return null;

        //Send messages
    }

    private void OnSessionStarted(Message msg)
    {
        Show();
    }

    private void OnGameResumed(Message msg)
    {
        Hide();
    }

    public void Show()
    {
        panel.SetActive(true);
        MessagePump.instance.QueueMsg(new PanelShowingMessage());
    }

    public void Hide()
    {
        panel.SetActive(false);
        MessagePump.instance.QueueMsg(new PanelHidingMessage());
    }
}
