﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RoundButton : MonoBehaviour
{
    public UnityEvent RoundOnClick;

    RectTransform rect;

    private void Awake()
    {
        rect = gameObject.GetComponent<RectTransform>();
    }

    public void OnClick()
    {
        float radius = rect.sizeDelta.x / 2;

        Vector2 mPos = Input.mousePosition;
        Vector2 bPos = rect.position;

        float dist = Mathf.Sqrt((mPos.x - bPos.x) * (mPos.x - bPos.x) + (mPos.y - bPos.y) * (mPos.y - bPos.y));

        if (dist < radius)
        {
            RoundOnClick.Invoke();
        }
    }
}
