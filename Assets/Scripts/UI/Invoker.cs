﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Invoker : MonoBehaviour
{
    public Button toInvoke;

    public void OnClick()
    {
        toInvoke.onClick.Invoke();
    }
}
