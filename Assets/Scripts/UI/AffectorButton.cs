﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AffectorButton : MonoBehaviour
{
    public Affector affector;
    public RectTransform rect;

    float radius = 0.0f;

    bool inSquare = false;
    bool inCircle;

    public void OnClick()
    {
        radius = rect.sizeDelta.x / 2;

        Vector2 mPos = Input.mousePosition;
        Vector2 bPos = rect.position;

        float dist = Mathf.Sqrt((mPos.x - bPos.x)*(mPos.x - bPos.x) + (mPos.y - bPos.y)*(mPos.y - bPos.y));

        if(dist < radius)
        {
            MessagePump.instance.QueueMsg(new ScreenTransitionMessage(Screen.SCREEN_AFFECTOR));

            inSquare = false;
            inCircle = false;
        }
    }

    private void Update()
    {
        if(inSquare)
        {
            radius = rect.sizeDelta.x / 2;

            Vector2 mPos = Input.mousePosition;
            Vector2 bPos = rect.position;

            float dist = Mathf.Sqrt((mPos.x - bPos.x) * (mPos.x - bPos.x) + (mPos.y - bPos.y) * (mPos.y - bPos.y));

            if (dist < radius)
            {
                if (!inCircle)
                {
                    inCircle = true;
                    MessagePump.instance.QueueMsg(new AffectorButtonStartHoverMessage(affector));
                }
            }
            else
            {
                if(inCircle)
                {
                    inCircle = false;
                    inSquare = false;
                    MessagePump.instance.QueueMsg(new AffectorButtonStopHoverMessage());
                }
            }
        }
    }

    public void OnStartHover()
    {
        inSquare = true;
    }
}
