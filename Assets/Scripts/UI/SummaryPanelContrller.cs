﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SummaryPanelContrller : MonoBehaviour
{
    public GameObject panel;

    public TextMeshProUGUI totalText;
    public TextMeshProUGUI changeText;
    public TextMeshProUGUI remainingText;

    public float startingEmissions;

    public Color goodColor;
    public Color badColor;

    public void Show(List<Affector> polluters)
    {
        if(panel.activeSelf)
        {
            return;
        }

        float sum = 0.0f;
        foreach(Affector a in polluters)
        {
            sum += a.GetCurrentEmissions();
        }
        totalText.text = sum.ToString("F2") + " MMT CO2 Eq";

        panel.SetActive(true);

        float difference = sum - startingEmissions;

        if(difference < 0)
        {
            changeText.color = goodColor;
        }
        else
        {
            changeText.color = badColor;
        }

        changeText.text = difference.ToString("F2") + " MMT CO2 Eq";

        remainingText.text = (-(StatsController.instance.currentEmissions - (StatsController.instance.startingEmissions * 0.20))).ToString("F2") + " MMT CO2 Eq";
        remainingText.color = badColor;
    }

    public void Hide()
    {
        panel.SetActive(false);
    }
}
