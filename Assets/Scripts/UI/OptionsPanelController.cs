﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsPanelController : MonoBehaviour
{
    public GameObject panel;

    public Slider UIAudioSlider;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        yield return null;

        //Register for messages
        yield return null;

        //Send messages
    }

    public void Show()
    {
        panel.SetActive(true);
        MessagePump.instance.QueueMsg(new PanelShowingMessage());
    }

    public void Hide()
    {
        panel.SetActive(false);
        MessagePump.instance.QueueMsg(new PanelHidingMessage());
    }

    public void OnUIAudioSliderChanged()
    {
        AudioController.instance.UIAudioSource.volume = UIAudioSlider.value/4;
    }
}
