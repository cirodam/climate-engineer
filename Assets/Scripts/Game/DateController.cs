﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DateController : MonoBehaviour
{
    public int currentMonth;
    public int currentYear;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_NEW_TURN, OnNewTurn);
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_SAVE_CREATED, OnGameSaveCreated);
        MessagePump.instance.Register(MessageType.MESSAGE_SAVE_STATE_LOADED, OnSaveStateLoaded);
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_STARTED, OnSessionStarted);
        yield return null;

        //Send messages

    }

    private void OnSessionStarted(Message msg)
    {
        currentMonth = 1;
        currentYear = 2017;
        MessagePump.instance.QueueMsg(new DateSetMessage(currentMonth, currentYear));
    }

    private void OnNewTurn(Message msg)
    {
        int oldMonth = currentMonth;
        int oldYear = currentYear;

        currentMonth++;

        if(currentMonth == 13)
        {
            currentMonth = 1;
            currentYear++;
        }

        MessagePump.instance.QueueMsg(new NewDateMessage(currentMonth, currentYear, oldMonth, oldYear));
    }

    private void OnGameSaveCreated(Message msg)
    {
        GameSaveCreatedMessage cMsg = (GameSaveCreatedMessage)msg;

        cMsg.gameSave.saves.Add(ToSaveState());
    }

    private void OnSaveStateLoaded(Message msg)
    {
        SaveStateLoadedMessage cMsg = (SaveStateLoadedMessage)msg;

        if (cMsg.saveState.saveName == "DateController")
        {
            FromSaveState((DateControllerSaveState)cMsg.saveState);
        }
    }

    private void FromSaveState(DateControllerSaveState save)
    {
        currentMonth = save.currentMonth;
        currentYear = save.currentYear;

        MessagePump.instance.QueueMsg(new DateSetMessage(currentMonth, currentYear));
    }

    private DateControllerSaveState ToSaveState()
    {
        DateControllerSaveState save = new DateControllerSaveState();

        save.saveName = "DateController";
        save.currentMonth = currentMonth;
        save.currentYear = currentYear;

        return save;
    }
}
