﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsController : MonoBehaviour
{
    public static StatsController instance;
    public float currentEmissions;
    public float startingEmissions;

    List<Affector> affectors;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            StartCoroutine(Init());
        }
        else
        {
            Destroy(gameObject);
        }
    }

    IEnumerator Init()
    {
        //Init 
        affectors = new List<Affector>();
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_AFFECTOR_INITIALIZED, OnAffectorInitialized);
        MessagePump.instance.Register(MessageType.MESSAGE_NEW_DATE, OnNewDate);
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_STARTED, OnSessionStarted);
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_ENDED, OnSessionEnded);
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_SAVE_CREATED, OnGameSaveCreated);
        MessagePump.instance.Register(MessageType.MESSAGE_SAVE_STATE_LOADED, OnSaveStateLoaded);
        yield return null;

        //Send messages
    }

    private void OnSessionStarted(Message msg)
    {
        currentEmissions = startingEmissions;
    }

    private void OnSessionEnded(Message msg)
    {
        currentEmissions = 0.0f;
    }

    private void OnAffectorInitialized(Message msg)
    {
        AffectorInitializedMessage cMsg = (AffectorInitializedMessage)msg;
        startingEmissions += cMsg.affector.baseEmissions;
        affectors.Add(cMsg.affector); 
    }

    private void OnNewDate(Message msg)
    {
        currentEmissions = 0.0f;

        foreach(Affector a in affectors)
        {
            currentEmissions += a.GetCurrentEmissions();
        }
    }

    private void OnGameSaveCreated(Message msg)
    {
        GameSaveCreatedMessage cMsg = (GameSaveCreatedMessage)msg;

        cMsg.gameSave.saves.Add(ToSaveState());
    }

    private void OnSaveStateLoaded(Message msg)
    {
        SaveStateLoadedMessage cMsg = (SaveStateLoadedMessage)msg;

        if(cMsg.saveState.saveName == "StatsController")
        {
            FromSaveState((StatsControllerSaveState)cMsg.saveState);
        }
    }

    private StatsControllerSaveState ToSaveState()
    {
        StatsControllerSaveState save = new StatsControllerSaveState();
        save.saveName = "StatsController";
        save.currentEmissions = currentEmissions;
        return save;
    }

    private void FromSaveState(StatsControllerSaveState save)
    {
        currentEmissions = save.currentEmissions;
    }
}
