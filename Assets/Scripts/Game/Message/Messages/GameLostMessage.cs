﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLostMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_GAME_LOST;
    }
}
