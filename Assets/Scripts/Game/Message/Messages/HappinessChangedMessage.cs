﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HappinessChangedMessage : Message
{
    public float happiness;

    public HappinessChangedMessage(float newHappiness)
    {
        happiness = newHappiness;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_HAPPINESS_CHANGED;
    }
}
