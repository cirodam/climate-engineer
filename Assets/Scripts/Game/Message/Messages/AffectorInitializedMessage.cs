﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AffectorInitializedMessage : Message
{
    public Affector affector;

    public AffectorInitializedMessage(Affector newAffector)
    {
        affector = newAffector;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_AFFECTOR_INITIALIZED;
    }
}
