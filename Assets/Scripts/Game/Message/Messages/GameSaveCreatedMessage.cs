﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSaveCreatedMessage : Message
{
    public GameSave gameSave;

    public GameSaveCreatedMessage(GameSave newSave)
    {
        gameSave = newSave;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_GAME_SAVE_CREATED;
    }
}
