﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SessionEndedMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_SESSION_ENDED;
    }
}
