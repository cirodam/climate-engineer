﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenTransitionMessage : Message
{
    public Screen screen;

    public ScreenTransitionMessage(Screen newScreen)
    {
        screen = newScreen;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_SCREEN_TRANSITION;
    }
}
