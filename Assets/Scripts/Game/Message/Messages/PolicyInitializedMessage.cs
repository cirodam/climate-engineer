﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolicyInitializedMessage : Message
{
    public Policy policy;

    public PolicyInitializedMessage(Policy newPolicy)
    {
        policy = newPolicy;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_POLICY_INITIALIZED;
    }
}
