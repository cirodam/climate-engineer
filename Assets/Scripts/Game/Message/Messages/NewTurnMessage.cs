﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewTurnMessage : Message
{
    public int turn;

    public NewTurnMessage(int newTurn)
    {
        turn = newTurn;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_NEW_TURN;
    }
}
