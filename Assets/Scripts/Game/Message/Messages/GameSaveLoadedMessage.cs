﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSaveLoadedMessage : Message
{
    public GameSave gameSave;

    public GameSaveLoadedMessage(GameSave newSave)
    {
        gameSave = newSave;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_GAME_SAVE_LOADED;
    }
}
