﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DateSetMessage : Message
{
    public int month;
    public int year;

    public DateSetMessage(int newMonth, int newYear)
    {
        month = newMonth;
        year = newYear;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_DATE_SET;
    }
}
