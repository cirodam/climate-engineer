﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolicyCancelledMessage : Message
{
    public Policy policy;

    public PolicyCancelledMessage(Policy newPolicy)
    {
        policy = newPolicy;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_POLICY_CANCELLED;
    }
}
