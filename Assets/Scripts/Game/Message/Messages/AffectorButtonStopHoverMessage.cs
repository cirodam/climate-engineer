﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AffectorButtonStopHoverMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_AFFECTOR_BUTTON_STOP_HOVER;
    }
}
