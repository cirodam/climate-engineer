﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliticalPowerChangeMessage : Message
{
    public float power;
    public float powerDelta;

    public PoliticalPowerChangeMessage(float newPower, float newDelta)
    {
        power = newPower;
        powerDelta = newDelta;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_POLITICAL_POWER_CHANGE;
    }
}
