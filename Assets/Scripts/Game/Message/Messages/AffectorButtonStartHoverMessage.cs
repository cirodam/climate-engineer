﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AffectorButtonStartHoverMessage : Message
{
    public Affector affector;

    public AffectorButtonStartHoverMessage(Affector newAffector)
    {
        affector = newAffector;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_AFFECTOR_BUTTON_START_HOVER;
    }
}
