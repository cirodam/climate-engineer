﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveStateLoadedMessage : Message
{
    public SaveState saveState;

    public SaveStateLoadedMessage(SaveState newState)
    {
        saveState = newState;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_SAVE_STATE_LOADED;
    }
}
