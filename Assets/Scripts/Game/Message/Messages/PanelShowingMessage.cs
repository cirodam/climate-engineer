﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelShowingMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_PANEL_SHOWING;
    }
}
