﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelHidingMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_PANEL_HIDING;
    }
}
