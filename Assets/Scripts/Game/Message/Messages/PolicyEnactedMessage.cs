﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolicyEnactedMessage : Message
{
    public Policy policy;

    public PolicyEnactedMessage(Policy newPolicy)
    {
        policy = newPolicy;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_POLICY_ENACTED;
    }
}
