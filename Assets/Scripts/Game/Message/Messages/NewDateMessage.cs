﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewDateMessage : Message
{
    public int month;
    public int year;

    public int oldMonth;
    public int oldYear;

    public NewDateMessage(int newMonth, int newYear, int newOldMonth, int newOldYear)
    {
        month = newMonth;
        year  = newYear;

        oldMonth = newOldMonth;
        oldYear = newOldYear;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_NEW_DATE;
    }
}

