﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SessionStartedMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_SESSION_STARTED;
    }
}
