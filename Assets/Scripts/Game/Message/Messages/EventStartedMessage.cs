﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventStartedMessage : Message
{
    public Event ev;

    public EventStartedMessage(Event newEv)
    {
        ev = newEv;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_EVENT_STARTED;
    }
}
