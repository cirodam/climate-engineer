﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliticalPowerDeltaChangeMessage : Message
{
    public float power;
    public float powerDelta;

    public PoliticalPowerDeltaChangeMessage(float newPower, float newPowerDelta)
    {
        power = newPower;
        powerDelta = newPowerDelta;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_POLITICAL_POWER_DELTA_CHANGE;
    }
}
