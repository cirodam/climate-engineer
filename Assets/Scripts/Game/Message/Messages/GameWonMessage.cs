﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameWonMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_GAME_WON;
    }
}
