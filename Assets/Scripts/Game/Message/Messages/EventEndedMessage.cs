﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventEndedMessage : Message
{
    public Event ev;

    public EventEndedMessage(Event newEv)
    {
        ev = newEv;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MESSAGE_EVENT_ENDED;
    }
}
