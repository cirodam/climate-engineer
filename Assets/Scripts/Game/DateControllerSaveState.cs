﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DateControllerSaveState : SaveState
{
    public int currentMonth;
    public int currentYear;
}
