﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class LoadController : MonoBehaviour
{
    public static LoadController instance;

    public FileInfo[] saveInfoList;

    BinaryFormatter bf;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            StartCoroutine(Init());
        }
        else
        {
            Destroy(gameObject);
        }
    }

    IEnumerator Init()
    {
        //Init 
        bf = new BinaryFormatter();
        LoadInfoList();

        yield return null;

        //Register for messages
        yield return null;

        //Send messages
    }

    public void LoadSave(string saveName)
    {
        FileStream fs = File.Open(Application.persistentDataPath + "/" + saveName, FileMode.Open);
        if(fs.Length == 0)
        {
            Debug.Log("Tried to load an empty save file");
        }
        else
        {
            GameSave gameSave = (GameSave)bf.Deserialize(fs);
            MessagePump.instance.QueueMsg(new GameSaveLoadedMessage(gameSave));
            MessagePump.instance.QueueMsg(new ScreenTransitionMessage(Screen.SCREEN_WORLD));

            foreach(SaveState s in gameSave.saves)
            {
                MessagePump.instance.QueueMsg(new SaveStateLoadedMessage(s));
            }
        }
    }

    public void DeleteSave(string saveName)
    {
        File.Delete(Application.persistentDataPath + "/" + saveName);
        LoadInfoList();
    }

    public void SaveGame(string saveName)
    {
        GameSave save = new GameSave();
        save.saveName = saveName;

        MessagePump.instance.QueueMsg(new GameSaveCreatedMessage(save));
        StartCoroutine(WaitAndSave(save));
    }

    IEnumerator WaitAndSave(GameSave g)
    {
        yield return null;
        FileStream fs = File.Create(Application.persistentDataPath + "/" + g.saveName + ".sv");

        bf.Serialize(fs, g);
        fs.Close();

        LoadInfoList();
    }

    public void LoadInfoList()
    {
        DirectoryInfo saveDir = new DirectoryInfo(Application.persistentDataPath);
        saveInfoList = saveDir.GetFiles();
    }
}
