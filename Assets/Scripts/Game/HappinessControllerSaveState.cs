﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HappinessControllerSaveState : SaveState
{
    public float currentHappiness;
    public List<Modifier> modifiers;
}
