﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DeltaMod
{
    public string modName;
    public float modValue;

    public DeltaMod(string newName, float newVal)
    {
        modName = newName;
        modValue = newVal;
    }
}
