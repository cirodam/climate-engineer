﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnController : MonoBehaviour
{
    public int currentTurn;

    public float turnLength;
    float elapsedTime;

    bool running;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_SAVE_CREATED, OnGameSaveCreated);
        MessagePump.instance.Register(MessageType.MESSAGE_SAVE_STATE_LOADED, OnSaveStateLoaded);
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_STARTED, OnSessionStarted);
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_ENDED, OnSessionEnded);
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_PAUSED, OnGamePaused);
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_RESUMED, OnGameResumed);
        yield return null;

        //Send messages
    }

    private void Update()
    {
        if(running)
        {
            if (elapsedTime >= turnLength)
            {
                NextTurn();
                elapsedTime = 0.0f;
            }
            else
            {
                elapsedTime += Time.deltaTime;
            }
        }
    }

    public void OnSessionStarted(Message msg)
    {
        running = false;
    }

    public void OnSessionEnded(Message msg)
    {
        currentTurn = 1;
        running = false;
    }

    public void OnGamePaused(Message msg)
    {
        running = false;
    }

    public void OnGameResumed(Message msg)
    {
        running = true;
    }

    public void NextTurn()
    {
        currentTurn++;

        MessagePump.instance.QueueMsg(new NewTurnMessage(currentTurn));
    }

    private void OnGameSaveCreated(Message msg)
    {
        GameSaveCreatedMessage cMsg = (GameSaveCreatedMessage)msg;

        cMsg.gameSave.saves.Add(ToSaveState());
    }

    private void OnSaveStateLoaded(Message msg)
    {
        SaveStateLoadedMessage cMsg = (SaveStateLoadedMessage)msg;

        if(cMsg.saveState.saveName == "TurnController")
        {
            FromSaveState((TurnControllerSaveState)cMsg.saveState);
        }
    }

    public void FromSaveState(TurnControllerSaveState save)
    {
        currentTurn = save.currentTurn;
    }

    public TurnControllerSaveState ToSaveState()
    {
        TurnControllerSaveState save = new TurnControllerSaveState();
        save.saveName = "TurnController";
        save.currentTurn = currentTurn;

        return save;
    }
}
