﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HappinessController : MonoBehaviour
{
    public static HappinessController instance;

    public float currentHapiness;
    public List<Modifier> modifiers;
    public ContinuousModifier decay;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            StartCoroutine(Init());
        }
        else
        {
            Destroy(gameObject);
        }
    }

    IEnumerator Init()
    {
        //Init 
        modifiers = new List<Modifier>();

        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_POLICY_ENACTED, OnPolicyEnacted);
        MessagePump.instance.Register(MessageType.MESSAGE_POLICY_CANCELLED, OnPolicyCancelled);
        MessagePump.instance.Register(MessageType.MESSAGE_EVENT_STARTED, OnEventStarted);
        MessagePump.instance.Register(MessageType.MESSAGE_EVENT_ENDED, OnEventEnded);
        MessagePump.instance.Register(MessageType.MESSAGE_NEW_DATE, OnNewDate);
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_STARTED, OnSessionStarted);
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_ENDED, OnSessionEnded);
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_SAVE_CREATED, OnGameSaveCreated);
        MessagePump.instance.Register(MessageType.MESSAGE_SAVE_STATE_LOADED, OnSaveStateLoaded);
        yield return null;

        //Send messages
        
    }

    private void OnSessionStarted(Message msg)
    {
        decay = new ContinuousModifier();
        decay.modName = "Climate Change Effects";
        decay.modDelta = 58;
        modifiers.Add(decay);

        CalculateHappiness();
    }

    private void OnSessionEnded(Message msg)
    {
        modifiers.Clear();
    }

    private void OnNewDate(Message msg)
    {
        decay.increment = -(StatsController.instance.currentEmissions/StatsController.instance.startingEmissions)*0.2f;
        decay.Update();
        CalculateHappiness();
    }

    private void OnPolicyEnacted(Message msg)
    {
        PolicyEnactedMessage cMsg = (PolicyEnactedMessage)msg;

        Modifier mod = new Modifier();

        mod.modName = cMsg.policy.policyName;
        mod.modDelta = cMsg.policy.currentSupport;

        modifiers.Add(mod);
        CalculateHappiness();

    }

    private void OnPolicyCancelled(Message msg)
    {
        PolicyCancelledMessage cMsg = (PolicyCancelledMessage)msg;

        foreach(Modifier m in modifiers)
        {
            if(m.modName == cMsg.policy.policyName)
            {
               modifiers.Remove(m);
               break;
            }
        }
    }

    private void OnEventStarted(Message msg)
    {
        EventStartedMessage cMsg = (EventStartedMessage)msg;

        foreach(EventModification m in cMsg.ev.modifications)
        {
            if(m.affector == "Happiness")
            {
                modifiers.Add(m.modifier);
            }
        }

        CalculateHappiness();
    }

    private void OnEventEnded(Message msg)
    {
        EventEndedMessage cMsg = (EventEndedMessage)msg;

        foreach(Modifier m in modifiers)
        {
            if(m.modName == cMsg.ev.eventName)
            {
                modifiers.Remove(m);
                break;
            }
        }

        CalculateHappiness();
    }

    private void CalculateHappiness()
    {
        float newHappy = 0.0f;

        newHappy += decay.modDelta;

        foreach (Modifier p in modifiers)
        {
            newHappy += p.modDelta;
        }
        newHappy /= (modifiers.Count + 1);

        if (newHappy != currentHapiness)
        {
            currentHapiness = newHappy;
            MessagePump.instance.QueueMsg(new HappinessChangedMessage(currentHapiness));
        }
    }

    private void OnGameSaveCreated(Message msg)
    {
        GameSaveCreatedMessage cMsg = (GameSaveCreatedMessage)msg;

        cMsg.gameSave.saves.Add(ToSaveState());
    }

    private void OnSaveStateLoaded(Message msg)
    {
        SaveStateLoadedMessage cMsg = (SaveStateLoadedMessage)msg;

        if(cMsg.saveState.saveName == "HappinessController")
        {
            FromSaveState((HappinessControllerSaveState)cMsg.saveState);
        }
        MessagePump.instance.QueueMsg(new HappinessChangedMessage(currentHapiness));
    }

    private void FromSaveState(HappinessControllerSaveState save)
    {
        currentHapiness = save.currentHappiness;
        modifiers = save.modifiers;
    }

    private HappinessControllerSaveState ToSaveState()
    {
        HappinessControllerSaveState save = new HappinessControllerSaveState();

        save.saveName = "HappinessController";
        save.currentHappiness = currentHapiness;
        save.modifiers = modifiers;

        return save;
    }
}
