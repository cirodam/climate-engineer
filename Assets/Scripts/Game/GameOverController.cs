﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverController : MonoBehaviour
{
    private List<Affector> affectors;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        affectors = new List<Affector>();
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_NEW_DATE, OnNewDate);
        MessagePump.instance.Register(MessageType.MESSAGE_AFFECTOR_INITIALIZED, OnAffectorInitialized);
        yield return null;

        //Send messages
    }

    private void OnAffectorInitialized(Message msg)
    {
        AffectorInitializedMessage cMsg = (AffectorInitializedMessage)msg;

        if(!affectors.Contains(cMsg.affector))
        {
            affectors.Add(cMsg.affector);
        }
    }

    private void OnNewDate(Message msg)
    {
        NewDateMessage cMsg = (NewDateMessage)msg;
  
        if(cMsg.month == 11 && cMsg.year % 4 == 0)
        {
            TestForLose();
        }
        TestForWin();
    }

    private void TestForLose()
    {
        if(HappinessController.instance.currentHapiness <= 50)
        {
            MessagePump.instance.QueueMsg(new GameLostMessage());
        }
    }

    private void TestForWin()
    {

        if(StatsController.instance.currentEmissions <= (StatsController.instance.startingEmissions*0.20))
        {
            MessagePump.instance.QueueMsg(new GameWonMessage());
        }
    }
}

