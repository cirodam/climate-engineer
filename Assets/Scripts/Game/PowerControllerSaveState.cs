﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PowerControllerSaveState : SaveState
{
    public float politicalPower;
    public float politicalPowerDelta;
    public List<DeltaMod> deltaMods;
}
