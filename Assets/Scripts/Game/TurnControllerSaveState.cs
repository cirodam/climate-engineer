﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TurnControllerSaveState : SaveState
{
    public int currentTurn;
}
