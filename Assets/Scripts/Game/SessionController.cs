﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SessionController : MonoBehaviour
{

    public SessionController instance;

    bool hovering;
    public bool paused;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            StartCoroutine(Init());
        }
        else
        {
            Destroy(gameObject);
        }
    }

    IEnumerator Init()
    {
        //Init 
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_ENDED, OnSessionEnded);
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_STARTED, OnSessionStarted);
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_SAVE_LOADED, OnGameSaveLoaded);
        yield return null;

        //Send messages
    }

    private void OnSessionStarted(Message msg)
    {
        paused = true;
    }

    private void OnSessionEnded(Message msg)
    {
        paused = false;
    }

    private void OnGameSaveLoaded(Message msg)
    {
        MessagePump.instance.QueueMsg(new GamePausedMessage());
        paused = true;
    }

    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity);

        if (hit.collider != null)
        {
            GameObject target = hit.collider.gameObject;
            hovering = true;
            MessagePump.instance.QueueMsg(new AffectorButtonStartHoverMessage(target.GetComponent<Affector>()));
        }
        else
        {
            if(hovering)
            {
                MessagePump.instance.QueueMsg(new AffectorButtonStopHoverMessage());
            }
        }

        if(Input.GetMouseButtonDown(0))
        {
            if(hovering)
            {
                MessagePump.instance.QueueMsg(new ScreenTransitionMessage(Screen.SCREEN_AFFECTOR));
            }
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(paused)
            {
                MessagePump.instance.QueueMsg(new GameResumedMessage());
                paused = false;
            }
            else
            {
                MessagePump.instance.QueueMsg(new GamePausedMessage());
                paused = true;
            }
        }
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public void PauseGame()
    {
        if(!paused)
        {
            paused = true;
            MessagePump.instance.QueueMsg(new GamePausedMessage());
        }
    }

    public void ResumeGame()
    {
        if(paused)
        {
            MessagePump.instance.QueueMsg(new GameResumedMessage());
            paused = false;
        }
    }
}
