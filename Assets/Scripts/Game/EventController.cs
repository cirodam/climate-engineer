﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventController : MonoBehaviour
{
    public List<Event> currentEvents;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        currentEvents = new List<Event>();
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_NEW_TURN, OnNewTurn);
        MessagePump.instance.Register(MessageType.MESSAGE_EVENT_STARTED, OnEventStarted);
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_ENDED, OnSessionEnded);
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_SAVE_CREATED, OnGameSaveCreated);
        MessagePump.instance.Register(MessageType.MESSAGE_SAVE_STATE_LOADED, OnSaveStateLoaded);
        yield return null;

        //Send messages
    }

    private void OnSessionEnded(Message msg)
    {
        currentEvents.Clear();
    }

    private void OnEventStarted(Message msg)
    {
        EventStartedMessage cMsg = (EventStartedMessage)msg;

        currentEvents.Add(cMsg.ev);
    }

    private void OnNewTurn(Message msg)
    {

        List<Event> toRemove = new List<Event>();
        foreach(Event e in currentEvents)
        {
            e.turnsRemaining--;

            if(e.turnsRemaining <= 0)
            {
                MessagePump.instance.QueueMsg(new EventEndedMessage(e));
                toRemove.Add(e);
            }
        }

        foreach(Event e in toRemove)
        {
            currentEvents.Remove(e);
        }
        toRemove.Clear();
    }

    private void OnGameSaveCreated(Message msg)
    {
        GameSaveCreatedMessage cMsg = (GameSaveCreatedMessage)msg;

        cMsg.gameSave.saves.Add(ToSaveState());
    }

    private void OnSaveStateLoaded(Message msg)
    {
        SaveStateLoadedMessage cMsg = (SaveStateLoadedMessage)msg;

        if(cMsg.saveState.saveName == "EventController")
        {
            FromSaveState((EventControllerSaveState)cMsg.saveState);
        }
    }

    private void FromSaveState(EventControllerSaveState save)
    {
        currentEvents = save.currentEvents;
    }

    private EventControllerSaveState ToSaveState()
    {
        EventControllerSaveState save = new EventControllerSaveState();
        save.saveName = "EventController";
        save.currentEvents = currentEvents;

        return save;
    }
}
