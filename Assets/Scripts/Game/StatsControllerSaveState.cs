﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StatsControllerSaveState : SaveState
{
    public float currentEmissions;
}
