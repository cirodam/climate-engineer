﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameSave 
{
    public string saveName;
    public string saveDate;

    public List<SaveState> saves;

    public GameSave()
    {
        saves = new List<SaveState>();
    }
}
