﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerController : MonoBehaviour
{
    public static PowerController instance;

    public float politicalPower;
    public float politicalPowerDelta;

    public List<DeltaMod> defaultMods;
    public List<DeltaMod> deltaMods;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            StartCoroutine(Init());
        }
        else
        {
            Destroy(gameObject);
        }

    }

    IEnumerator Init()
    {
        //Init 
        deltaMods = new List<DeltaMod>();
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_NEW_DATE, OnNewDate);
        MessagePump.instance.Register(MessageType.MESSAGE_POLICY_ENACTED, OnPolicyEnacted);
        MessagePump.instance.Register(MessageType.MESSAGE_POLICY_CANCELLED, OnPolicyCancelled);
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_SAVE_CREATED, OnGameSaveCreated);
        MessagePump.instance.Register(MessageType.MESSAGE_SAVE_STATE_LOADED, OnSaveStateLoaded);
        MessagePump.instance.Register(MessageType.MESSAGE_HAPPINESS_CHANGED, OnHappinessChanged);
        MessagePump.instance.Register(MessageType.MESSAGE_EVENT_STARTED, OnEventStarted);
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_STARTED, OnSessionStarted);
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_ENDED, OnSessionEnded);
        yield return null;

        //Send messages
    }

    private void OnSessionStarted(Message msg)
    {
        MessagePump.instance.QueueMsg(new PoliticalPowerChangeMessage(politicalPower, politicalPowerDelta));

        if(defaultMods != null)
        {
            foreach(DeltaMod d in defaultMods)
            {
                deltaMods.Add(d);
            }
        }
        CalculateDelta();
    }

    private void OnSessionEnded(Message msg)
    {
        deltaMods.Clear();
    }

    private void OnEventStarted(Message msg)
    {
        EventStartedMessage cMsg = (EventStartedMessage)msg;

        switch(cMsg.ev.eventName)
        {
            case "Fundraising":
                politicalPower += 1000;
                break;

            case "Speech":
                politicalPower -= 400;
                break;
        }

        MessagePump.instance.QueueMsg(new PoliticalPowerChangeMessage(politicalPower, politicalPowerDelta));
    }

    private void OnHappinessChanged(Message msg)
    {
        HappinessChangedMessage cMsg = (HappinessChangedMessage)msg;

        
        deltaMods[1].modValue = (cMsg.happiness - 50.0f) / 2.0f;
        CalculateDelta();
    }

    private void OnNewDate(Message msg)
    {
        politicalPower += politicalPowerDelta;
        MessagePump.instance.QueueMsg(new PoliticalPowerChangeMessage(politicalPower, politicalPowerDelta));
    }

    private void OnPolicyEnacted(Message msg)
    {
        PolicyEnactedMessage Cmsg = (PolicyEnactedMessage)msg;

        SpendPower(Cmsg.policy.policyCost);
        if(Cmsg.policy.deltaCost != 0)
        {
            deltaMods.Add(new DeltaMod(Cmsg.policy.policyName, -Cmsg.policy.deltaCost));
            CalculateDelta();
        }
    }

    private void OnPolicyCancelled(Message msg)
    {
        PolicyCancelledMessage cMsg = (PolicyCancelledMessage)msg;

        foreach(DeltaMod d in deltaMods)
        {
            if(d.modName == cMsg.policy.policyName)
            {
                deltaMods.Remove(d);
                break;
            }
        }
        CalculateDelta();
    }

    public void SpendPower(float value)
    {
        politicalPower -= value;
        MessagePump.instance.QueueMsg(new PoliticalPowerChangeMessage(politicalPower, politicalPowerDelta));
    }

    private void CalculateDelta()
    {
        float newDelta = 0.0f;

        foreach(DeltaMod p in deltaMods)
        {
            newDelta += p.modValue;
        }

        if(newDelta != politicalPowerDelta)
        {
            politicalPowerDelta = newDelta;
            MessagePump.instance.QueueMsg(new PoliticalPowerDeltaChangeMessage(politicalPower, politicalPowerDelta));
        }
    }

    public void AddDeltaMod(DeltaMod target)
    {

        deltaMods.Add(target);

        CalculateDelta();
    }

    private void OnGameSaveCreated(Message msg)
    {
        GameSaveCreatedMessage cMsg = (GameSaveCreatedMessage)msg;

        cMsg.gameSave.saves.Add(ToSaveState());
    }

    private void OnSaveStateLoaded(Message msg)
    {
        SaveStateLoadedMessage cMsg = (SaveStateLoadedMessage)msg;

        if(cMsg.saveState.saveName == "PowerController")
        {
            FromSaveState((PowerControllerSaveState)cMsg.saveState);
        }
    }

    private PowerControllerSaveState ToSaveState()
    {
        PowerControllerSaveState save = new PowerControllerSaveState();

        save.saveName = "PowerController";
        save.politicalPower = politicalPower;
        save.politicalPowerDelta = politicalPowerDelta;
        save.deltaMods = deltaMods;

        return save;
    }

    private void FromSaveState(PowerControllerSaveState save)
    {
        politicalPower = save.politicalPower;
        politicalPowerDelta = save.politicalPowerDelta;
        deltaMods = save.deltaMods;
    }
}
