﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PolicySaveState : SaveState
{
    public float policyCost;
    public float deltaCost;
    public float publicSupport;
    public bool enacted;
}
