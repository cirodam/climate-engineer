﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolicyController : MonoBehaviour
{
    public GameObject world;
    Dictionary<string, Policy> policies;

    private void Awake()
    {
        policies = new Dictionary<string, Policy>();

        foreach (Transform child in world.transform)
        {
            Policy policy = child.GetComponent<Policy>();

            if (policy != null)
            {
                policies.Add(policy.name, policy);
            }
        }
    }
}
