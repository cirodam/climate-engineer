﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Modification
{
    public string affector;
    public Modifier modifier;
    public bool remove;
}

public class Policy : MonoBehaviour
{
    public string policyName;
    public string categoryName;

    public TextAsset policyDesc;

    public float policyCost;
    public float deltaCost;
    public float publicSupport;
    public float currentSupport;
    public int supportSpeed;

    public bool enacted;

    public List<Modification> modList;
    public Dictionary<string, List<Modification>> modifications;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        if(supportSpeed == 0)
        {
            supportSpeed = 1;
        }

        currentSupport = publicSupport;
        modifications = new Dictionary<string, List<Modification>>();

        foreach(Modification m in modList)
        {
            if(!modifications.ContainsKey(m.affector))
            {
                modifications.Add(m.affector, new List<Modification>());
            }

            modifications[m.affector].Add(m);
        }
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_SAVE_CREATED, OnGameSaveCreated);
        MessagePump.instance.Register(MessageType.MESSAGE_SAVE_STATE_LOADED, OnSaveStateLoaded);
        MessagePump.instance.Register(MessageType.MESSAGE_NEW_DATE, OnNewDate);
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_ENDED, OnSessionEnded);
        yield return null;

        //Send messages
        MessagePump.instance.QueueMsg(new PolicyInitializedMessage(this));
    }

    private void OnSessionEnded(Message msg)
    {
        enacted = false;
    }

    private void OnNewDate(Message msg)
    {
        currentSupport += -(HappinessController.instance.decay.increment/2);

        if (currentSupport > 100)
        {
            currentSupport = 100;
        }
    }

    public void OnGameSaveCreated(Message msg)
    {
        GameSaveCreatedMessage cMsg = (GameSaveCreatedMessage)msg;

        cMsg.gameSave.saves.Add(ToSaveState());
    }

    public void OnSaveStateLoaded(Message msg)
    {
        SaveStateLoadedMessage cMsg = (SaveStateLoadedMessage)msg;

        if(cMsg.saveState.saveName == policyName)
        {
            FromSaveState((PolicySaveState)cMsg.saveState);
        }
    }

    public void FromSaveState(PolicySaveState save)
    {
        policyCost = save.policyCost;
        deltaCost = save.deltaCost;
        publicSupport = save.publicSupport;
        enacted = save.enacted;
    }

    public PolicySaveState ToSaveState()
    {
        PolicySaveState save = new PolicySaveState();

        save.saveName = policyName;
        save.policyCost = policyCost;
        save.deltaCost = deltaCost;
        save.publicSupport = publicSupport;
        save.enacted = enacted;

        return save;
    }
}
