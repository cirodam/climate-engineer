﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct EventModification
{
    public string affector;
    public Modifier modifier;

    public EventModification(string newAffector, Modifier newMod)
    {
        affector = newAffector;
        modifier = newMod;
    }
}

[System.Serializable]
public class Event
{
    public string eventName;
    public string eventType;
    public string eventDesc;

    public int turnsRemaining;
    public bool popup;

    public List<EventModification> modifications;

    public Event()
    {
        modifications = new List<EventModification>();
    }
}
