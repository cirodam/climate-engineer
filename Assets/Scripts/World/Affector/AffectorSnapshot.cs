﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AffectorSnapshot
{
    public int month;
    public int year;
    public float currentEmissions;

    public AffectorSnapshot(int newMonth, int newYear)
    {
        month = newMonth;
        year = newYear;
    }
}
