﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Affector : MonoBehaviour
{
    public string affectorName;
    public Color affectorColor;

    public List<AffectorSnapshot> history;
    public Modifier baseline;
    public Dictionary<string, Modifier> modifiers;

    public float baseEmissions;
    public float currentEmissions;
    public float currentDelta;

    private void Awake()
    {
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        //Init 
        history          = new List<AffectorSnapshot>();
        modifiers        = new Dictionary<string, Modifier>();
        yield return null;

        //Register for messages
        MessagePump.instance.Register(MessageType.MESSAGE_NEW_DATE, onNewDate);
        MessagePump.instance.Register(MessageType.MESSAGE_POLICY_ENACTED, OnPolicyEnacted);
        MessagePump.instance.Register(MessageType.MESSAGE_POLICY_CANCELLED, OnPolicyCancelled);
        MessagePump.instance.Register(MessageType.MESSAGE_GAME_SAVE_CREATED, OnGameSaveCreated);
        MessagePump.instance.Register(MessageType.MESSAGE_SAVE_STATE_LOADED, OnSaveStateLoaded);
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_STARTED, OnSessionStarted);
        MessagePump.instance.Register(MessageType.MESSAGE_SESSION_ENDED, OnSessionEnded);
        yield return null;

        //Send messages
        MessagePump.instance.QueueMsg(new AffectorInitializedMessage(this));
    }

    private void OnSessionStarted(Message msg)
    {
        currentEmissions = baseEmissions;
        modifiers.Add(baseline.modName, baseline);
    }

    private void onNewDate(Message msg)
    {
        NewDateMessage cMsg = (NewDateMessage)msg;

        //Create a snapshot of the last month
        AffectorSnapshot lastMonth = new AffectorSnapshot(cMsg.oldMonth, cMsg.oldYear);
        lastMonth.currentEmissions = currentEmissions;
        history.Add(lastMonth);

        //Recalculate currentEmissions
        currentDelta = 0.0f;
        foreach (Modifier mod in modifiers.Values)
        {
            currentDelta += mod.modDelta;
        }

        currentEmissions += currentDelta;

        if (currentEmissions < 0.0f)
        {
            currentEmissions = 0.0f;
        }
    }

    private void OnPolicyEnacted(Message msg)
    {
        PolicyEnactedMessage cMsg = (PolicyEnactedMessage)msg;

        if(cMsg.policy.modifications.ContainsKey(affectorName))
        {
            List<Modification> mods = cMsg.policy.modifications[affectorName];

            foreach (Modification m in mods)
            {
                if (m.remove)
                {
                    RemoveModifier(m.modifier);
                }
                else
                {
                    AddModifier(m.modifier);
                }
            }
        }
    }


    private void OnPolicyCancelled(Message msg)
    {
        PolicyCancelledMessage cMsg = (PolicyCancelledMessage)msg;

        if (cMsg.policy.modifications.ContainsKey(affectorName))
        {
            List<Modification> mods = cMsg.policy.modifications[affectorName];

            foreach (Modification m in mods)
            {
                if(m.remove)
                {
                    AddModifier(m.modifier);
                }
                else
                {
                    RemoveModifier(m.modifier);
                }
            }
        }
    }

    private void AddModifier(Modifier mod)
    {
        if(modifiers.ContainsKey(mod.modName))
        {
            modifiers[mod.modName] = mod;
        }
        else
        {
            modifiers.Add(mod.modName, mod);
        }
    }

    private void RemoveModifier(Modifier mod)
    {
        if (modifiers.ContainsKey(mod.modName))
        {
            modifiers.Remove(mod.modName);
        }
    }

    private void OnSessionEnded(Message msg)
    {
        history.Clear();
        modifiers.Clear();
        currentEmissions = 0.0f;
    }

    public float GetCurrentEmissions()
    {
        return currentEmissions;
    }

    private void OnGameSaveCreated(Message msg)
    {
        GameSaveCreatedMessage cMsg = (GameSaveCreatedMessage)msg;
        
        cMsg.gameSave.saves.Add(ToSaveState());
    }

    private void OnSaveStateLoaded(Message msg)
    {
        SaveStateLoadedMessage cMsg = (SaveStateLoadedMessage)msg;

        if(cMsg.saveState.saveName == affectorName)
        {
            FromSaveState((AffectorSaveState)cMsg.saveState);
        }
    }

    private AffectorSaveState ToSaveState()
    {
        AffectorSaveState saveState = new AffectorSaveState();

        saveState.saveName = affectorName;
        saveState.history = history;
        saveState.currentEmissions = currentEmissions;
        List<Modifier> toSave = new List<Modifier>();
        foreach(string m in modifiers.Keys)
        {
            toSave.Add(modifiers[m]);
        }
        saveState.modifiers = toSave;

        return saveState;
    }

    private void FromSaveState(AffectorSaveState saveState)
    {
        history   = saveState.history;
        currentEmissions = saveState.currentEmissions;
        foreach (Modifier m in saveState.modifiers)
        {
            if(!modifiers.ContainsKey(m.modName))
            {
                modifiers.Add(m.modName, m);
            }
        }
    }
}
