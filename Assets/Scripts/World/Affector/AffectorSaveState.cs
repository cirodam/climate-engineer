﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AffectorSaveState : SaveState
{
    public List<AffectorSnapshot> history;
    public List<Modifier> modifiers;
    public float currentEmissions;
}
