﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ContinuousModifier : Modifier
{
    public float increment;

    public void Update()
    {
        modDelta += increment;
    }
}
