﻿using System;
using UnityEngine;

namespace AwesomeCharts {

    public abstract class AxisBaseChart<T> : BaseChart<T> where T : AxisChartData {
        [SerializeField]
        private XAxis xAxis;

        [SerializeField]
        private YAxis yAxis;

        public XAxis XAxis {
            get { return xAxis; }
            set {
                xAxis = value;
                SetDirty();
            }
        }

        public YAxis YAxis {
            get { return yAxis; }
            set {
                yAxis = value;
                SetDirty();
            }
        }

        protected XAxisRenderer xAxisRenderer;
        protected YAxisRenderer yAxisRenderer;

        protected virtual void OnUpdateAxis() { }

        protected virtual void Awake() {
            if (xAxis == null)
                xAxis = new XAxis();

            if (yAxis == null)
                yAxis = new YAxis();
        }

        protected override void OnInstantiateViews() {
            base.OnInstantiateViews();

            xAxisRenderer = InstantiateXAxisRenderer();
            xAxisRenderer.transform.SetSiblingIndex(0);
            yAxisRenderer = InstantiateYAxisRenderer();
            yAxisRenderer.transform.SetSiblingIndex(1);
        }

        protected virtual XAxisRenderer InstantiateXAxisRenderer() {
            return viewCreator.InstantiateXAxisRenderer("XAxis", contentView.transform, PivotValue.BOTTOM_LEFT);
        }

        protected virtual YAxisRenderer InstantiateYAxisRenderer() {
            return viewCreator.InstantiateYAxisRenderer("YAxis", contentView.transform, PivotValue.BOTTOM_LEFT);
        }

        protected override void OnUpdateViewsSize(Vector2 size) {
            base.OnUpdateViewsSize(size);

            xAxisRenderer.GetComponent<RectTransform>().sizeDelta = size;
            yAxisRenderer.GetComponent<RectTransform>().sizeDelta = size;
        }

        protected override void OnDrawChartContent() {
            base.OnDrawChartContent();
            UpdateAxis();
        }

        private void UpdateAxis() {
            if (GetChartData() == null)
                return;

            AxisBounds axisBounds = GetAxisBounds();

            xAxisRenderer.Axis = XAxis;
            xAxisRenderer.axisMinValue = axisBounds.XMin;
            xAxisRenderer.axisMaxValue = axisBounds.XMax;
            xAxisRenderer.customLabelValues = GetChartData().CustomLables;

            yAxisRenderer.Axis = YAxis;
            yAxisRenderer.axisMinValue = axisBounds.YMin;
            yAxisRenderer.axisMaxValue = axisBounds.YMax;

            OnUpdateAxis();
            xAxisRenderer.Invalidate();
            yAxisRenderer.Invalidate();
        }

        protected AxisBounds GetAxisBounds() {
            float xMin = XAxis.AutoAxisValue ? GetChartData().GetMinPosition() : XAxis.MinAxisValue;
            float xMax = XAxis.AutoAxisValue ? GetChartData().GetMaxPosition() : XAxis.MaxAxisValue;
            float yMin = YAxis.AutoAxisValue ? GetChartData().GetMinValue() : YAxis.MinAxisValue;
            float yMax = YAxis.AutoAxisValue ? GetClosestRoundValue(GetChartData().GetMaxValue()) : YAxis.MaxAxisValue;

            return new AxisBounds(xMin, xMax, yMin, yMax);
        }

        private int GetClosestRoundValue(float value) {
            if (value <= 0f)
                return 100;

            float valueWithMargin = value * 1.1f;
            float log10 = Mathf.FloorToInt(Mathf.Log10(valueWithMargin) + 1);
            float roundedValue = Mathf.Ceil(valueWithMargin);
            if (log10 > 2) {
                roundedValue = ((int)(roundedValue / Mathf.Pow(10, log10 - 2)) + 1) * Mathf.Pow(10, log10 - 2);
            } else if (log10 >= 1) {
                roundedValue = ((int)(roundedValue / Mathf.Pow(10, log10 - 1)) + 1) * Mathf.Pow(10, log10 - 1);
            }

            return (int)roundedValue;
        }
    }
}
