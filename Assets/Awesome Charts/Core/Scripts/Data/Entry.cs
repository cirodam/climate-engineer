﻿using UnityEngine;

namespace AwesomeCharts {

    [System.Serializable]
    public class Entry {

        [SerializeField]
        private float value;

        public Entry () {
            this.value = 0f;
        }

        public Entry (float value) {
            this.value = value;
        }

        public float Value {
            get { return Mathf.Max (value, 0); }
            set { this.value = value; }
        }
    }
}